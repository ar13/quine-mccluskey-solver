package QuineMcCluskey;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class QM_GUI {

	private JFrame frame;
	private JPanel backgroundPanel, mainPanel, resultPanel;
	private JSpinner spinner;
	private JButton solveBtn;
	private JTextField minTermsTxt, dontCaresTxt;
	private JTextArea primeImplicantsArea, minPossibilitiesArea;
	private Color backgroundColor = new Color(200, 13, 13, 10);

	/**
	 * Create the application.
	 */
	public QM_GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(570, 200, 182, 268);
		
		setBackgroundPanel();
		setMainPanel();
		setResultPanel();
		
		frame.setVisible(true);
	}
	
	private void setBackgroundPanel() {
		backgroundPanel = new JPanel(new CardLayout());
		backgroundPanel.setBackground(backgroundColor);
		frame.getContentPane().add(backgroundPanel, BorderLayout.CENTER);
	}
	
	private void setMainPanel() {
		mainPanel = new JPanel(null);
		
		JLabel numberOfVariables = new JLabel("Number of variables");
		numberOfVariables.setFont(new Font("Calibri", Font.BOLD, 11));
		numberOfVariables.setBounds(10, 11, 140, 18);
		mainPanel.add(numberOfVariables);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(1, 1, 26, 1));
		spinner.setFont(new Font("Calibri", Font.BOLD, 12));
		spinner.setBounds(125, 10, 41, 20);
		JSpinner.DefaultEditor spinnerEditor = (DefaultEditor) spinner.getEditor();
		spinnerEditor.getTextField().setEditable(false);
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		mainPanel.add(spinner);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.BLACK);
		separator.setBounds(10, 40, 156, 2);
		mainPanel.add(separator);

		JLabel noteLabel = new JLabel("Separate numbers with spaces");
		noteLabel.setHorizontalAlignment(SwingConstants.CENTER);
		noteLabel.setFont(new Font("Calibri", Font.PLAIN, 11));
		noteLabel.setBounds(10, 49, 156, 14);
		mainPanel.add(noteLabel);
		
		JLabel minTermsLbl = new JLabel("Min-terms");
		minTermsLbl.setFont(new Font("Calibri", Font.BOLD, 11));
		minTermsLbl.setBounds(10, 74, 67, 23);
		mainPanel.add(minTermsLbl);
		
		minTermsTxt = new JTextField();
		minTermsTxt.setFont(new Font("Calibri", Font.PLAIN, 13));
		minTermsTxt.setHorizontalAlignment(SwingConstants.CENTER);
		minTermsTxt.setBounds(10, 92, 156, 31);
		mainPanel.add(minTermsTxt);
		minTermsTxt.setColumns(10);
		
		JLabel dontCaresLbl = new JLabel("Don't Cares");
		dontCaresLbl.setFont(new Font("Calibri", Font.BOLD, 11));
		dontCaresLbl.setBounds(10, 134, 67, 23);
		mainPanel.add(dontCaresLbl);
		
		dontCaresTxt = new JTextField();
		dontCaresTxt.setFont(new Font("Calibri", Font.PLAIN, 13));
		dontCaresTxt.setHorizontalAlignment(SwingConstants.CENTER);
		dontCaresTxt.setBounds(10, 152, 156, 31);
		mainPanel.add(dontCaresTxt);
		dontCaresTxt.setColumns(10);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		separator_1.setBounds(10, 192, 156, 2);
		mainPanel.add(separator_1);
		
		solveBtn = new JButton("Solve");
		solveBtn.setFont(new Font("Calibri", Font.BOLD, 14));
		solveBtn.setBounds(10, 205, 156, 23);
		solveBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		mainPanel.add(solveBtn);

		backgroundPanel.add("mainPanel", mainPanel);
	}
	
	private void setResultPanel() {
		resultPanel = new JPanel(null);
		setBackBtn();
		setTextAreas();
		backgroundPanel.add("resultPanel", resultPanel);
	}
	
	private void setBackBtn() {
		JButton backBtn = new JButton();
		backBtn.setToolTipText("back");
		backBtn.setBounds(5, 5, 24, 24);
		
		backBtn.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				showPanel("mainPanel");
			}
			
		    public void mouseEntered(MouseEvent me) {
		    	backBtn.setIcon(new ImageIcon("./icons/backBtnHover.png"));
		    }

		    public void mouseExited(MouseEvent me) {
		    	backBtn.setIcon(new ImageIcon("./icons/backBtn.png"));
		    }
		});
		
		backBtn.setOpaque(false);
		backBtn.setContentAreaFilled(false);
		backBtn.setBorderPainted(false);
		backBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		backBtn.setIcon(new ImageIcon("./icons/backBtn.png"));
		resultPanel.add(backBtn);
	}
	
	private void setTextAreas() {
		primeImplicantsArea = new JTextArea();
		primeImplicantsArea.setFont(new Font("Calibri", Font.PLAIN, 13));
		primeImplicantsArea.setForeground(Color.BLACK);
		primeImplicantsArea.setBackground(Color.WHITE);
		primeImplicantsArea.setBounds(5, 35, 385, 130);
		primeImplicantsArea.setMargin(new Insets(5, 5, 5, 5));
		primeImplicantsArea.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(primeImplicantsArea);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(50);
		scrollPane.getVerticalScrollBar().setUnitIncrement(50);
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(0, 0, 385, 130);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 35, 385, 130);
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(300, 400));
		panel.add(scrollPane);
		resultPanel.add(panel);
		
		minPossibilitiesArea = new JTextArea();
		minPossibilitiesArea.setFont(new Font("Calibri", Font.PLAIN, 13));
		minPossibilitiesArea.setForeground(Color.BLACK);
		minPossibilitiesArea.setBackground(Color.WHITE);
		minPossibilitiesArea.setEditable(false);
		minPossibilitiesArea.setBounds(5, 170, 385, 97);
		minPossibilitiesArea.setMargin(new Insets(5, 5, 5, 5));
		
		JScrollPane scrollPane1 = new JScrollPane(minPossibilitiesArea);
		scrollPane1.getHorizontalScrollBar().setUnitIncrement(50);
		scrollPane1.getVerticalScrollBar().setUnitIncrement(50);
		scrollPane1.setBackground(Color.WHITE);
		scrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane1.setBounds(0, 0, 385, 97);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(5, 170, 385, 97);	
		panel_1.setLayout(null);
		panel_1.setPreferredSize(new Dimension(300, 400));
		panel_1.add(scrollPane1);
		resultPanel.add(panel_1);
		
	}
	
	public void showPanel(final String panelName) {
		if (panelName == "mainPanel") {
			frame.setBounds(frame.getBounds().x, frame.getBounds().y, 182, 268);
		} else {
			frame.setBounds(frame.getBounds().x, frame.getBounds().y, 400, 300);
		}
		((CardLayout) backgroundPanel.getLayout()).next(backgroundPanel);
	}
	
	public JButton getSolveBtn() {
		return solveBtn;
	}
	
	public String getMinTermsTxt() {
		return minTermsTxt.getText();
	}
	
	public String getDontCaresTxt() {
		return dontCaresTxt.getText();
	}
	
	public int getSpinnerValue() {
		return (int) spinner.getValue();
	}
	
	public JTextArea getPrimeImplicantsArea() {
		return primeImplicantsArea;
	}
	
	public void setPrimeImplicantsAreaTxt(final String txt) {
		primeImplicantsArea.setText(txt);
	}
	
	public JTextArea getMinPossArea() {
		return minPossibilitiesArea;
	}
	
	public void setMinPossAreaTxt(final String txt) {
		minPossibilitiesArea.setText(txt);
	}
}
