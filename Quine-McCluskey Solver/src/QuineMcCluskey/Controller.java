package QuineMcCluskey;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Controller {
	
	static QM_GUI app_gui;
	static boolean showResults = true;
	public static void main(String[] args) {
		app_gui = new QM_GUI();
		setSolveBtnListener();
	}
	
	private static void setSolveBtnListener() {
		app_gui.getSolveBtn().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				checkAndProcessData();
				
				if (showResults) {
					app_gui.showPanel("resultPanel");
				}
			}
		});
	}
	
	private static void checkAndProcessData() {
		app_gui.setPrimeImplicantsAreaTxt("");
		app_gui.setMinPossAreaTxt("");
		final int varsNum = app_gui.getSpinnerValue();
		
		// minterms.
		String min = app_gui.getMinTermsTxt();
		Scanner m = new Scanner(min);
		int[] temp1 = new int[100000];
		m.useDelimiter("[^\\d*\\-*]+");
		int counter1 = 0;
		while(m.hasNext()){
			temp1[counter1] = Integer.parseInt(m.next());
			counter1++;
		}
		int[] minTerms = new int[counter1]; // # minTerms
		
		for(int j = 0; j < counter1; j++){
			minTerms[j] = temp1[j];
		}
		m.close();
		
		//=====================================
		
		// don't cares.
		
		String dont = app_gui.getDontCaresTxt();
		Scanner d = new Scanner(dont);
		int[] temp2 = new int[100000];
		d.useDelimiter("[^\\d*\\-*]+");
		int counter2 = 0;
		while(d.hasNext()){
			temp2[counter2] = Integer.parseInt(d.next());
			counter2++;
		}
		int[] dontCare = new int[counter2]; // # dontCares
		
		for(int j = 0; j < counter2; j++){
			dontCare[j] = temp2[j];
		}
		d.close();
		
		if(minTerms.length == 0){
			showResults = false;
			JOptionPane.showMessageDialog(null, "No min-terms are entered.");
		} else {
			showResults = true;
			// print the results
			QM qm = new QM(minTerms, dontCare, varsNum);
			
			boolean check = true;
			
			for(int i = 0; i < qm.mixed.length - 1; i++){
				for(int j = i + 1; j < qm.mixed.length; j++){
					if(qm.mixed[i] == qm.mixed[j] || qm.mixed[i] < 0 || qm.mixed[i] > (int) Math.pow(2, varsNum) - 1){
						check = false;
						break;
					}
				}
			}
			
			if(qm.mixed[qm.mixed.length - 1] < 0 || qm.mixed[qm.mixed.length - 1] > (int) Math.pow(2, varsNum) - 1){
				check = false;
			}
			
			if(!check){
				showResults = false;
				JOptionPane.showMessageDialog(null, "You entered repeated numbers or numbers not between 0 and 2^n - 1 inclusive\n"
						+ "where n is the number of variables.");
			}
			
			else{
				if(qm.mixed.length == (int) Math.pow(2, varsNum)){
					app_gui.getMinPossArea().append("F = 1");
				}
				
				else{
					// Processing.

					List binRepresentations = qm.process();
					List primeImplicantsWithRepetitions = new List();
					List minTermsInvolved = new List();

					for (int i = 0; i < binRepresentations.size(); i++) {
						List a = ((List) binRepresentations.get(i));
						for (int j = 0; j < a.size(); j++) {
							if (((QM.matchedPairs) a.get(j)).check == false) {
								String str = ((QM.matchedPairs) a.get(j)).pattern;
								primeImplicantsWithRepetitions.add(str);
								List b = new List();
								b = ((QM.matchedPairs) a.get(j)).pairs;
								minTermsInvolved.add(b);
							}
						}
					}

					List.Node N = primeImplicantsWithRepetitions.getHead();
					String[] toArray = new String[primeImplicantsWithRepetitions.size()];

					for (int i = 0; i < toArray.length; i++) {
						toArray[i] = (String) N.getElement();
						N = N.getNext();
					}

					// Processing.

					// ==================================================================

					// Remove Duplicated Prime Implicants.
					List.Node K = primeImplicantsWithRepetitions.getHead();
					List filterTerms = new List();
					boolean flag;
					int finalSize = 0;
					for (int i = 0; i < toArray.length; i++) {
						String temp = (String) K.getElement();
						flag = false;
						for (int j = 0; j < i; j++) {
							if (temp.equals(toArray[j])) {
								filterTerms.add(i);
								flag = true;
								break;
							}
						}
						if (flag == false) {
							toArray[finalSize] = temp;
							finalSize++;
						}
						K = K.getNext();
					}

					String[] primeImplicants = new String[finalSize];

					for (int i = 0; i < finalSize; i++) {
						primeImplicants[i] = toArray[i];
					}

					for (int i = filterTerms.size() - 1; i >= 0; i--) {
						minTermsInvolved.remove((int) filterTerms.get(i));
					}
					// Remove Duplicated Prime Implicants.

					// ==================================================================

					// convert the prime implicants from binary to letters.

					for (int i = 0; i < primeImplicants.length; i++) {
						primeImplicants[i] = qm.toLetters(primeImplicants[i]);
					}

					// convert the prime implicants from binary to letters.

					// ==================================================================

					// Creating the final table.

					int[][] table = new int[primeImplicants.length + 1][minTerms.length + 1];
					for (int i = 1; i < primeImplicants.length + 1; i++) {
						table[i][0] = i - 1;
					}
					for (int i = 1; i < minTerms.length + 1; i++) {
						table[0][i] = minTerms[i - 1];
					}

					qm.primeImplicants = new String[primeImplicants.length];
					for (int i = 0; i < primeImplicants.length; i++) {
						qm.primeImplicants[i] = primeImplicants[i];
					}

					// Creating the final table

					// ==================================================================

					// Displaying Prime Implicants.
					
					app_gui.getPrimeImplicantsArea().append("Consider the Function as: F(");
					String Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
					for (int i = 0; i < qm.numOfVariables; i++) {
						app_gui.getPrimeImplicantsArea().append(Character.toString(Letters.charAt(i)));
						if (i < qm.numOfVariables - 1) {
							app_gui.getPrimeImplicantsArea().append(", ");
						}
					}
					app_gui.getPrimeImplicantsArea().append(")\n");
					app_gui.getPrimeImplicantsArea().append("Then,\n");
					app_gui.getPrimeImplicantsArea().append("\nPrime Implicants:\n\n");
					for (int i = 0; i < primeImplicants.length; i++) {
						app_gui.getPrimeImplicantsArea().append((i + 1) + ") ");
						app_gui.getPrimeImplicantsArea().append(primeImplicants[i]);
						app_gui.getPrimeImplicantsArea().append(" Matched minTerms: ");
						for (int j = 0; j < ((List) (minTermsInvolved.get(i))).size(); j++) {
							int x = (int) ((List) (minTermsInvolved.get(i))).get(j);
							app_gui.getPrimeImplicantsArea().append("m" + x + " ");
							for (int k = 1; k < minTerms.length + 1; k++) {
								if (x == table[0][k]) {
									table[i + 1][k] = 1;
								}
							}
							if (j < ((List) (minTermsInvolved.get(i))).size() - 1) {
								app_gui.getPrimeImplicantsArea().append("- ");
							}
						}
						app_gui.getPrimeImplicantsArea().append("\n");
					}
					app_gui.getPrimeImplicantsArea().append("\n\n");
					
					// Displaying Prime Implicants.

					// ==================================================================

					qm.table = new int[table.length][table[0].length];
					for (int i = 0; i < table.length; i++) {
						for (int j = 0; j < table[0].length; j++) {
							qm.table[i] = table[i];
						}
					}

					// test minimum
					qm.allPossible();

					
					
					
					app_gui.getMinPossArea().append("Minimum functions:\n\n");

					for (int i = 0; i < qm.minimum.size(); i++) {
						app_gui.getMinPossArea().append("F" + (i + 1) + " = ");
						for (int j = 0; j < ((List) qm.minimum.get(i)).size(); j++) {
							app_gui.getMinPossArea().append(primeImplicants[(int) (((List) qm.minimum.get(i)).get(j))]);
							if (j < ((List) qm.minimum.get(i)).size() - 1) {
								app_gui.getMinPossArea().append(" + ");
							}
						}
						app_gui.getMinPossArea().append("\n");
					}
					app_gui.getMinPossArea().append("\n\n");
				}
			}
		}
	}
}
